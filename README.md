# kics-remote-ruleset

This project contains the SAST ruleset for the demo project [kics-remote-custom-ruleset](https://gitlab.com/gitlab-org/security-products/demos/kics-remote-custom-ruleset).

That project uses the `.gitlab/sast-ruleset.toml` in this project to configure the `kics` SAST-IaC scanner in that project.